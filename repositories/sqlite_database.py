import logging
import sqlite3


class Database:
    logger = logging.getLogger(__name__)

    class Decorators:
        @staticmethod
        def log(function):
            def wrapper(self, *args, **kwargs):
                try:
                    return function(self, *args, **kwargs)
                except sqlite3.DatabaseError:
                    self.logger.error(f'DatabaseError executing {function.__name__}{args} {kwargs}', exc_info=True)

            return wrapper

    def __init__(self, database_file_path):
        self.__connection = sqlite3.connect(database_file_path)
        self.__connection.row_factory = self.__dict_factory

    @staticmethod
    def __dict_factory(cursor, row):
        d = dict()
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    @Decorators.log
    def insert_case(self, city: str, latitude: float, longitude: float):
        self.__connection.cursor().execute(
            'INSERT INTO cases(city, latitude, longitude) VALUES(?, ?, ?)', (city, latitude, longitude))
        self.__connection.commit()

    @Decorators.log
    def select_cases(self, city: str):
        cursor = self.__connection.cursor().execute('SELECT COUNT(*) FROM cases WHERE city = ? ', (city,))
        return cursor.fetchone()

    @Decorators.log
    def select_countries(self):
        cursor = self.__connection.cursor().execute('SELECT * FROM countries')
        return cursor.fetchall()

    @Decorators.log
    def select_country_by_code(self, code: str):
        cursor = self.__connection.cursor().execute('SELECT * FROM countries WHERE code = ?', (code,))
        return cursor.fetchall()

    @Decorators.log
    def close_connection(self):
        self.__connection().close()
