import logging
import requests


class CoronavirusMonitor:
    logger = logging.getLogger(__name__)

    def __init__(self, headers):
        self.headers = headers

    def get_cases_by_country(self, country):
        try:
            r = requests.get(
                url='https://coronavirus-monitor.p.rapidapi.com/coronavirus/latest_stat_by_country.php',
                headers=self.headers,
                params=dict(country=country)
            )
            return r.json()
        except requests.exceptions.HTTPError as e:
            self.logger.error(f'HTTPError {e.response.status_code} getting cases coronavirus: {e.response.text}')
        except requests.exceptions.RequestException:
            self.logger.error('RequestException getting cases coronavirus', exc_info=True)
