import logging
import requests


class GeoCodeApi:
    logger = logging.getLogger(__name__)

    def __init__(self, headers):
        self.headers = headers

    def get_nearest_cities(self, latitude: float, longitude: float, range_in_meter: int):
        try:
            r = requests.get(
                url='https://geocodeapi.p.rapidapi.com/GetNearestCities',
                headers=self.headers,
                params=dict(latitude=latitude, longitude=longitude, range=range_in_meter)
            )
            return r.json()
        except requests.exceptions.HTTPError as e:
            self.logger.error(f'HTTPError {e.response.status_code} getting nearest cities: {e.response.text}')
        except requests.exceptions.RequestException:
            self.logger.error('RequestException getting nearest cities', exc_info=True)
