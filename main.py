import os
import atexit
from repositories import Database

from utils import Bot, setup_logging, Config

if __name__ == '__main__':
    path = os.path.dirname(os.path.abspath(__file__))
    # Init logging
    setup_logging(os.path.join(path, 'config', 'logging.yaml'))
    # Init config and bot messages
    config = Config(os.path.join(path, 'config', 'config.json'))
    messages = Config(os.path.join(path, 'config', 'messages.json'))
    # Init SQLite database
    database = Database('data.db')
    # Init and run the bot
    bot = Bot(config, messages, database)
    bot.run()
    atexit.register(database.close_connection())
