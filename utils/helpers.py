import difflib as dl


def get_countries_dict(database):
    countries = database.select_countries()
    return countries


def get_country_by_name(lang, country_name, countries_dict):
    country_name = country_name.capitalize()
    countries = (c[lang] for c in countries_dict)
    found_country = dl.get_close_matches(country_name, countries, 1)
    if not found_country:
        return '', ''
    found_country = next(filter(lambda c: c[lang] == found_country[0], countries_dict))
    return found_country[lang], found_country['api_name']
