import logging

from telegram import InlineKeyboardMarkup, InlineKeyboardButton, error, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import Updater, CommandHandler, ConversationHandler, CallbackQueryHandler, Filters, MessageHandler

from utils.helpers import get_countries_dict, get_country_by_name
from repositories import CoronavirusMonitor, GeoCodeApi

SELECT_COUNTRY, SEND_LOCATION, SELECT_CITY = range(3)


class Bot:
    def __init__(self, config, messages, database):
        self.config = config
        self.messages = messages
        self.database = database
        self.logger = logging.getLogger(__name__)
        self.countries_dict = get_countries_dict(database)
        self.coronavirus_monitor = CoronavirusMonitor(config['rapid-api']['coronavirus-monitor'])
        self.geocode_api = GeoCodeApi(config['rapid-api']['geocodeapi'])

    class Decorators:
        @staticmethod
        def check_if_language_is_selected(function):
            def wrapper(self, update, context):
                lang = context.user_data.get('lang')
                if not lang:
                    self.language(update, context)
                    return ConversationHandler.END
                try:
                    return function(self, update, context)
                except error.TelegramError:
                    self.logger.error(f'TelegramError executing {function.__name__}', exc_info=True)

            return wrapper

    def start(self, update, context):
        self.language(update, context)

    def language(self, update, context):
        keyboard = InlineKeyboardMarkup([
            [InlineKeyboardButton('English 🇺🇸', callback_data='en')],
            [InlineKeyboardButton('Русский 🇷🇺', callback_data='ru')],
            [InlineKeyboardButton('Українська 🇺🇦', callback_data='ua')],
        ])
        update.message.reply_text(self.messages['choose_lang'], reply_markup=keyboard)

    def save_language(self, update, context):
        query = update.callback_query
        context.user_data['lang'] = query.data
        query.message.reply_text(self.messages[query.data]['start']['greeting'])
        return ConversationHandler.END

    @Decorators.check_if_language_is_selected
    def country(self, update, context):
        lang = context.user_data.get('lang')
        update.message.reply_text(self.messages[lang]['country']['enter_country'])
        return SELECT_COUNTRY

    @Decorators.check_if_language_is_selected
    def select_country(self, update, context):
        lang = context.user_data.get('lang')
        country_name = update.message.text
        country_name, api_country_name = get_country_by_name(lang, country_name, self.countries_dict)
        if not country_name or not api_country_name:
            update.message.reply_text(self.messages[lang]['country']['wrong_input'])
            return ConversationHandler.END
        data = self.coronavirus_monitor.get_cases_by_country(api_country_name)['latest_stat_by_country'][0]
        update.message.reply_text(self.messages[lang]['country']['result'].format(
            data['record_date'][:16], country_name, data['total_cases']))
        return ConversationHandler.END

    @Decorators.check_if_language_is_selected
    def location(self, update, context):
        lang = context.user_data.get('lang')
        reply_markup = ReplyKeyboardMarkup([
            [KeyboardButton(text=self.messages[lang]['location']['send_location_button'], request_location=True)]
        ])
        context.bot.send_message(update.message.chat.id, text=self.messages[lang]['location']['send_location'],
                                 reply_markup=reply_markup)
        return SEND_LOCATION

    @Decorators.check_if_language_is_selected
    def sand_location(self, update, context):
        lang = context.user_data.get('lang')
        location = update.message.location

        country_id = self.geocode_api.get_nearest_cities(location.latitude, location.longitude, 0)[0]['CountryId']

        country = self.database.select_country_by_code(country_id)
        # TODO: Uncomment
        # country_name, country_api_name = country[lang], country['api_name']
        country_name, country_api_name = 'Ukraine', 'UA'

        data = self.coronavirus_monitor.get_cases_by_country(country_api_name)['latest_stat_by_country'][0]

        update.message.reply_text(self.messages[lang]['country']['result'].format(
            data['record_date'][:16], country_name, data['total_cases']))
        return ConversationHandler.END

    @Decorators.check_if_language_is_selected
    def city(self, update, context):
        lang = context.user_data.get('lang')
        update.message.reply_text(self.messages[lang]['city']['enter_city'])
        return SELECT_COUNTRY

    @Decorators.check_if_language_is_selected
    def select_city(self, update, context):
        lang = context.user_data.get('lang')
        city_name = update.message.text
        total_cases = self.database.select_cases(city_name)
        if not total_cases:
            update.message.reply_text(self.messages[lang]['city']['wrong_input'])
            return ConversationHandler.END
        update.message.reply_text(
            self.messages[lang]['city']['result'].format(city_name, total_cases))
        return ConversationHandler.END

    @Decorators.check_if_language_is_selected
    def help(self, update, context):
        lang = context.user_data.get('lang')
        update.message.reply_text(self.messages[lang]['help'])

    @Decorators.check_if_language_is_selected
    def about(self, update, context):
        lang = context.user_data.get('lang')
        update.message.reply_text(self.messages[lang]['about'])

    @staticmethod
    def cancel(update, context):
        update.message.reply_text('Пук!')
        return ConversationHandler.END

    def error(self, update, context):
        """Log Errors caused by Updates."""
        self.logger.warning('Update "%s" caused error "%s"', update, context.error)

    def run(self):
        updater = Updater(self.config['bot']['token'], use_context=True)
        dp = updater.dispatcher

        country_conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('country', self.country, pass_user_data=True)],
            states={
                SELECT_COUNTRY: [MessageHandler(Filters.text, self.select_country)]
            },
            fallbacks=[CommandHandler('cancel', self.cancel)]
        )

        location_conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('location', self.location, pass_user_data=True)],
            states={
                SEND_LOCATION: [MessageHandler(Filters.all, self.sand_location)]
            },
            fallbacks=[CommandHandler('cancel', self.cancel)]
        )

        city_conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('city', self.city, pass_user_data=True)],
            states={
                SELECT_COUNTRY: [MessageHandler(Filters.text, self.select_country)]
            },
            fallbacks=[CommandHandler('cancel', self.cancel)]
        )

        dp.add_handler(CommandHandler('start', self.start))
        dp.add_handler(CommandHandler('lang', self.language))
        dp.add_handler(CommandHandler('help', self.help, pass_user_data=True))
        dp.add_handler(CommandHandler('about', self.about, pass_user_data=True))
        dp.add_handler(CallbackQueryHandler(self.save_language, pass_user_data=True))

        # /country
        dp.add_handler(country_conversation_handler)
        # /location
        dp.add_handler(location_conversation_handler)
        # /city
        dp.add_handler(city_conversation_handler)

        dp.add_error_handler(self.error)

        self.logger.info('Start polling...')
        updater.start_polling()
        updater.idle()
